import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/pages/Home';
import People from '@/components/pages/People';
import Contact from '@/components/pages/Contact';
import Error from '@/components/pages/Error';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/people',
            name: 'people',
            component: People,
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact,
        },
        {
            path: '*',
            name: 'error',
            component: Error,
        },
    ],
});
