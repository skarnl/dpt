import Vue from 'vue';
import * as svgicon from 'vue-svgicon';
import VeeValidate from 'vee-validate';

import App from './App';
import router from './router';

// Import the scss
// eslint-disable-next-line no-unused-vars
import style from './assets/css/global.scss';

Vue.config.productionTip = false;

// Default tag name is 'svgicon'
Vue.use(svgicon, {
    tagName: 'svgicon',
});
Vue.use(VeeValidate);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>',
});

// TODO add svg4everbody
// TODO add object-fit-fix
