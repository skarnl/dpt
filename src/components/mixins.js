export const DelayedCallMixin = {
    data() {
        return {
            loaded: false,
        };
    },

    mounted() {
        this.getDelayedData();
    },

    methods: {
        loadData() {
            throw new TypeError('loadData should be implemented');
        },

        // slightly delayed call, to make sure we have a little loading animation
        getDelayedData() {
            this.loaded = false;

            // add promise to delay the refresh of the data
            const timerPromise = new Promise((resolve) => {
                setTimeout(() => {
                    resolve();
                }, 1300);
            });

            Promise.all([timerPromise, this.loadData()]).then(() => {
                this.dataLoaded();
            });
        },

        dataLoaded() {
            this.loaded = true;
        },
    },
};
