/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'arrow': {
    width: 8,
    height: 13,
    viewBox: '0 0 8 13',
    data: '<path pid="0" d="M7.322 7.018S8 6.491 8 6.031c0-.44-.678-.988-.678-.988l-.01-.01L1.932.261C1.49-.087.77-.087.33.26c-.44.348-.44.918 0 1.266L4.835 6.03.33 10.533c-.44.35-.44.918 0 1.267.44.348 1.16.348 1.601 0l5.381-4.773.01-.009" _fill="#FFF" fill-rule="evenodd"/>'
  }
})
