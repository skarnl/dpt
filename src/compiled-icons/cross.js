/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'cross': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M12 14.121l-9.546 9.546-2.121-2.121L9.879 12 .333 2.454 2.454.333 12 9.879 21.546.333l2.121 2.121L14.121 12l9.546 9.546-2.121 2.121L12 14.121z" _fill="#FFF" fill-rule="evenodd"/>'
  }
})
