/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'menu': {
    width: 24,
    height: 22,
    viewBox: '0 0 24 22',
    data: '<path pid="0" d="M0 0h24v3H0V0zm0 9.5h24v3H0v-3zm0 9h24v3H0v-3z" _fill="#FFF" fill-rule="evenodd"/>'
  }
})
