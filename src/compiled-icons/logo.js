/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'logo': {
    width: 32,
    height: 32,
    viewBox: '0 0 32 32',
    data: '<path pid="0" d="M0 25.573L25.6-.03 32 6.373 6.4 31.971z" _fill="#FFF" fill-rule="evenodd"/>'
  }
})
