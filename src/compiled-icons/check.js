/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'check': {
    width: 24,
    height: 19,
    viewBox: '0 0 24 19',
    data: '<path pid="0" d="M8.206 18.117l-.38.38L0 10.67l2.608-2.608 5.36 5.36L21.393 0 24 2.608 8.348 18.26l-.142-.143z" _fill="#7BDB4C" fill-rule="evenodd"/>'
  }
})
